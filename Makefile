SHELL = /bin/bash
SOURCE = szeto
FILES = $(SOURCE)/*.py $(SOURCE)/test/*.py

.PHONY: all install test

all: test install unit_tests linting type_checking

install: setup.py $(SOURCE)/*.py
	python setup.py install --user

test: pytest.log

pytest.log: $(SOURCE)/*.py $(SOURCE)/test/*.py
	set -o pipefail && pytest \
	--cov=$(SOURCE) \
	--cov-report term-missing \
	--pythonwarnings ignore::DeprecationWarning \
	--pythonwarnings ignore::UserWarning \
	--flake8 --mypy | tee $@

unit_tests: $(FILES)
	pytest \
	--pythonwarnings ignore::DeprecationWarning \
	--pythonwarnings ignore::UserWarning \
	--cov-report term-missing \
	--cov=$(SOURCE)

linting: $(FILES)
	pytest --flake8 -W ignore::DeprecationWarning

type_checking: $(FILES)
	pytest --mypy -W ignore::DeprecationWarning
