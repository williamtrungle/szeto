# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Add a logger and verbose levels
- Add a model checkpoint and metrics/hparams logging
- Implement debug levels for training unit tests

## [0.1.1] - 2021-01-14
### Added
- GitLab CI/CD

### Modified
- Split Makefile test rules into unit tests, linting and type checking for 
  parallel tests on CI/CD runner

## [0.1.0] - 2021-01-14
### Added
- Experiment module: Pytorch-Lightning Module and DataModuel trainer for 
  repeated trials with different seeds, with gpu support for deep learning 
- Pytest-based testing of the Experiment on the CIFAR dataset
- Flake8 tests for code style/linting
- Mypy tests for type hints
- Readme: title and short description of Project SZETO
- License: MTI
- Changelog: versions kept on GitLab
- Setuptools: make package installable
- Makefile: commands for installing package from source and running tests

[Unreleased]: https://gitlab.com/williamtrungle/szeto/-/compare/0.1.1...master
[0.1.1]: https://gitlab.com/williamtrungle/szeto/-/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/williamtrungle/szeto/-/tags/0.1.0
