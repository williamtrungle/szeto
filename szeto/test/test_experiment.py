# type: ignore

import pytest
import torch
import shutil
import numpy as np
import pytorch_lightning as pl

from szeto.experiment import Experiment
from pl_bolts.datamodules.cifar10_datamodule import (
    TinyCIFAR10DataModule
)


class Model(pl.LightningModule):
    """Minimal 10 class classification"""
    def __init__(self, dims, **_):
        super().__init__()
        self.dims = dims
        self.model = torch.nn.Sequential(
                torch.nn.Flatten(),
                torch.nn.Linear(np.prod(dims), 10),
                torch.nn.ReLU(inplace=True))
        self.cost = torch.nn.CrossEntropyLoss()

    def forward(self, x):
        return self.model(x)

    def configure_optimizers(self):
        return torch.optim.SGD(self.parameters(), lr=0.01)

    def step(self, batch, stage=None):
        x, true = batch
        pred = self(x)
        loss = self.cost(pred, true)
        if stage:
            self.log(f'{stage}_loss', loss)
        return loss

    def training_step(self, batch, batch_idx):
        return self.step(batch)

    def validation_step(self, batch, batch_idx):
        return self.step(batch, 'test')


def CIFAR():
    return lambda seed, data_dir='data', batch_size=32, **kwargs: (
            TinyCIFAR10DataModule(
                seed=seed,
                data_dir=data_dir,
                batch_size=batch_size))


# FIXTURES

@pytest.fixture(scope='session')
def model():
    return Model


@pytest.fixture(scope='session')
def dataset():
    return CIFAR()


@pytest.fixture
def experiment(model, dataset):
    experiment = Experiment(model, dataset, batch_size=32)
    yield experiment
    for seed in set(experiment.seeds):
        shutil.rmtree(experiment.log_dir/f"{seed}")


# TESTS

def test_notrials(experiment):
    with pytest.raises(ValueError):
        experiment(trials=0)


def test_trials(experiment):
    mean, variance, support = experiment()
    assert isinstance(mean, float) and mean > 0, f"{mean=} <= 0"
    assert isinstance(variance, float) and variance == 0, f"{variance=} != 0"
    assert support == 1, f"{support=} != 1"


@pytest.mark.timeout(60, method='thread')
def test_seeds(experiment):
    mean, variance, support = experiment(0, 1, 42)
    assert mean > 0.0, f"{mean=} <= 0.0"
    assert variance > 0.0, f"{variance=} <= 0.0"
    assert support == 3, f"{support=} != 3"
    assert experiment.seeds == [0, 1, 42], f"{experiment.seeds=} != [0, 1, 42]"


@pytest.mark.timeout(60, method='thread')
def test_auto_seed_finder(experiment):
    assert experiment(0, 1), "resume False, different seeds"
    with pytest.raises(FileExistsError):
        experiment(0, 0)
    experiment.resume = True
    assert experiment(0, 1), "resume=True, different seeds"
    assert experiment(0, 0), "resume True, same seeds"
