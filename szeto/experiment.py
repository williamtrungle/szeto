import secrets
import pytorch_lightning as pl

from pathlib import Path
from typing import List, Tuple, Union, Optional, Type
from statistics import mean, variance


class Experiment:
    """Deep learning model training and evaluation

    This class takes care of tracking, logging and checkpointing experiments
    while also setting up environment configurations and repeating tests for
    statistical considerations.

    @returns mean, variance, support
    """

    def __init__(self,
                 model: Type[pl.LightningModule],
                 datamodule: Type[pl.LightningDataModule],
                 *,
                 inputs: str = 'X',
                 targets: str = 'y',
                 cross_validation: bool = None,
                 monitor: str = 'test_loss',
                 log_dir: Union[Path, str] = 'log',
                 data_dir: Union[Path, str] = 'data',
                 gpus: Optional[str] = None,
                 accelerator: Optional[str] = None,
                 precision: int = 32,
                 deterministic: bool = False,
                 benchmark: bool = False,
                 resume: bool = False,
                 jobs: int = 0,
                 verbose: int = 0,
                 debug: int = 0,
                 name: str = "neuralnetwork",
                 batch_size: int = 1,
                 learning_rate: float = 1.0,
                 epochs: int = 1,
                 **hparams):
        self._model = model
        self._datamodule = datamodule
        self.inputs = inputs
        self.targets = targets
        self.cross_validation = cross_validation
        self.monitor = monitor
        self.log_dir = Path(log_dir)
        self.data_dir = Path(data_dir)
        self.gpus = gpus
        self.accelerator = accelerator
        self.precision = precision
        self.deterministic = deterministic
        self.benchmark = benchmark
        self.resume = False
        self.jobs = jobs
        self.verbose = verbose
        self.debug = debug
        self.seeds: List[int] = []
        self.hparams = {
            **hparams,
            "name": name,
            "batch_size": batch_size,
            "learning_rate": learning_rate,
            "epochs": epochs,
        }

    def seed(self, init: Optional[int] = None) -> Tuple[int, Path]:
        while True:
            seed = init if init is not None else secrets.randbelow(1 << 32)
            path = self.log_dir/f"{seed}"/self.targets
            if init is None and path.exists():
                continue
            elif not self.resume and path.exists():
                raise FileExistsError(
                        f"{path} exists already, set resume=True to ignore")
            self.seeds.append(pl.seed_everything(seed))
            return seed, path

    def __call__(self, *seeds: Optional[int], trials: int = 1):
        results = []
        if not seeds:
            if not trials:
                raise ValueError(f"Training with {trials=} and {seeds=}")
            seeds = (None,) * trials
        for trial, seed in enumerate(seeds):
            seed, log_dir = self.seed(seed)
            log_dir.mkdir(parents=True, exist_ok=True)
            checkpoint: Optional[str]
            try:
                checkpoint = str(sorted(log_dir.glob(
                    f"{self.hparams['name']}"
                    f"-{seed}"
                    f"-{self.targets}*.ckpt"))[0])
            except IndexError:
                checkpoint = None
            datamodule = self.datamodule(seed=seed)
            model = self.model(seed, datamodule.dims, checkpoint)
            trainer = self.trainer(checkpoint)
            trainer.tune(model=model, datamodule=datamodule)
            trainer.fit(model=model, datamodule=datamodule)
            results.append(trainer.logged_metrics[self.monitor].item())
        return (mean(results),
                variance(results) if len(results) > 1 else 0.0,
                len(results))

    def model(self,
              seed: int, dims: Tuple[int, ...],
              checkpoint: Optional[str] = None) -> pl.LightningModule:
        if checkpoint:
            return self._model.load_from_checkpoint(checkpoint)
        else:
            return self._model(seed=seed, dims=dims, **self.hparams)

    def datamodule(self, seed: int) -> pl.LightningDataModule:
        return self._datamodule(  # type: ignore
            seed=seed,
            target=self.targets,
            batch_size=self.hparams['batch_size'],
            jobs=self.jobs,
            data_dir=self.data_dir,
        )

    def trainer(self, checkpoint: Optional[str] = None) -> pl.Trainer:
        return pl.Trainer(
            resume_from_checkpoint=checkpoint,
            gpus=self.gpus,
            accelerator=self.accelerator,
            precision=self.precision,
            benchmark=self.benchmark,
            deterministic=self.deterministic,
            max_epochs=self.hparams['epochs'],
            logger=False,
            checkpoint_callback=False,
        )
