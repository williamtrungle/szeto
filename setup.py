from setuptools import setup  # type: ignore

if __name__ == '__main__':
    with open("README.md") as readme, \
         open("szeto/__init__.py") as version, \
         open("requirements.txt") as requirements:
        setup(
            name='szeto',
            version=version.readline().strip(),
            description="Rigourous deep learning experimentation",
            license="MIT",
            long_description=readme.read(),
            author="William Le",
            author_email="williamtrungle@gmail.com",
            packages=['szeto'],
            python_requires='>=3.8',
            install_requires=requirements.readlines())
